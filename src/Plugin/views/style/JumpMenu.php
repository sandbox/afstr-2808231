<?php

namespace Drupal\jump_menu_style_plugin\Plugin\views\style;

use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\taxonomy\Entity;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Url;

use Drupal\jump_menu\Form\JumpMenuForm;
use Drupal\jump_menu\Form\JumpMenuOptions;

/**
 * Style plugin to render each item in a jump menu
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "jump_menu_style_plugin",
 *   title = @Translation("Jump Menu"),
 *   help = @Translation("Displays rows in a View as a Jump Menu."),
 *   theme = "views_view_list",
 *   display_types = {"normal"}
 * )
 */
class JumpMenu extends StylePluginBase {

  /**
   * Does the style plugin allows to use style plugins.
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Set default options
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['type'] = array('default' => 'select');
    $options['class'] = array('default' => '');
    $options['wrapper_class'] = array('default' => 'item-list');

    return $options;
  }

  /**
   * Render the given style.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // These form options are not implemented yet. (From the D7 version)
    $form['path'] = array(
      '#type' => 'select',
      '#title' => t('Path field'),
      '#options' => $options,
      '#default_value' => $this->options['path'],
    );

    $form['hide'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide the "Go" button'),
      '#default_value' => !empty($this->options['hide']),
      '#description' => t('If hidden, this button will only be hidden for users with javascript and the page will automatically jump when the select is changed.'),
    );

    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Button text'),
      '#default_value' => $this->options['text'],
    );

    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Selector label'),
      '#default_value' => $this->options['label'],
      '#description' => t('The text that will appear as the the label of the selector element. If blank no label tag will be used.'),
    );

    $form['choose'] = array(
      '#type' => 'textfield',
      '#title' => t('Choose text'),
      '#default_value' => $this->options['choose'],
      '#description' => t('The text that will appear as the selected option in the jump menu.'),
    );

    $form['inline'] = array(
      '#type' => 'checkbox',
      '#title' => t('Set this field to display inline'),
      '#default_value' => !empty($this->options['inline']),
    );

    $form['default_value'] = array(
      '#type' => 'checkbox',
      '#title' => t('Select the current contextual filter value'),
      '#default_value' => !empty($this->options['default_value']),
      '#description' => t('If checked, the current path will be displayed as the default option in the jump menu, if applicable.'),
    );
  }

  public function render() {
    $formBuilder = \Drupal::formBuilder();

    $form = new JumpMenuForm();
    $form_state = new FormState();
    $select = JumpMenuOptions::create();

    // inject the options here for jump menu form

    /**
    * @var \Drupal\views\ResultRow $row
    */
    $row = NULL;
    foreach ($this->view->result as $row) {
      $select->addOption($row->_entity->label(),
        Url::fromUri('entity:' . $row->_entity->getEntityTypeId() . '/' . $row->_entity->id()));
    }

    $form_state->addBuildInfo('args', [$select]);

    return $formBuilder->buildForm($form, $form_state);
  }
}